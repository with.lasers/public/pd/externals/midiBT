# Makefile to build class 'helloworld' for Pure Data.
# Needs Makefile.pdlibbuilder as helper makefile for platform-dependent build
# settings and rules.



# library name
lib.name = midiBt



#as the linux target is assumed to bela - so we have some specifics to handle
target.triplet := $(subst -, ,$(shell $(CC) -dumpmachine))
ifneq ($(filter linux gnu% kfreebsd, $(target.triplet)),)
	  system = Linux
endif

# input source file (class name == source file basename)

# hfile := MidiBT/common.h

ifeq ($(system), Linux)

cflags += -I/usr/include/glib-2.0 -I/usr/lib/arm-linux-gnueabihf/glib-2.0/include -lglib-2.0
ldlibs += -lgio-2.0 -lgobject-2.0 -lglib-2.0

bluetooth.class.sources := MidiBT/bluetooth_linux.c

else

bluetooth.class.sources := MidiBT/bluetooth_stub.c

endif

# all extra files to be included in binary distribution of the library
datafiles = README.md

# extra files

# from https://github.com/porres/pd-else/blob/master/Makefile - re abstractions iuncluded as well!
# extrafiles = \
# $(wildcard Code_source/Abstractions/control/*.pd) \
# $(wildcard Code_source/Abstractions/audio/*.pd) \
# $(wildcard Code_source/Abstractions/extra_abs/*.pd) \
# $(wildcard Code_source/Compiled/extra_source/*.tcl) \
# $(wildcard Documentation/Help-files/*.pd) \
# $(wildcard Documentation/extra_files/*.*) \
# $(wildcard *.txt) \
# Documentation/README.pdf



ifeq ($(system), Linux)
#	echo "settings for LINUX"
	PDLIBDIR=/root/Bela/projects/pd-externals
	PDINCLUDEDIR=/usr/local/include/libpd/
endif


# include Makefile.pdlibbuilder from submodule directory 'pd-lib-builder'
PDLIBBUILDER_DIR=pd-lib-builder/
include $(PDLIBBUILDER_DIR)/Makefile.pdlibbuilder
