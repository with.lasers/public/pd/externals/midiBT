#include <stdbool.h>
#include <string.h>

#include "m_pd.h"
#include "../common.h"

#include <glib.h>
#include <gio/gio.h>




//Status Codes

#define STATUS_INIT = -1
#define STATUS_INERROR = -99
#define STATUS_CONNECTING = 0
#define STATUS_CONNECTED = 1 



static t_class *bluetooth_class = NULL;

typedef struct _bluetooth {
  t_object x_obj;

  //Current state;
  t_symbol *mac_address;
  float status_code;

  //DBus things
  GDBusConnection *dbusConnection;
  GDBusProxy *adapterProxy;
  GDBusProxy *propsProxy;

  //Outlets
  t_outlet *status_outlet;
  
} t_adapter;  


void adapter_bang(t_adapter *x) {
  (void)x; // silence unused variable warning
  
  //return the current status code in the status outlet
  outlet_float(x->status_outlet, x->status_code);

}  



//Handle scripted commands to Bluetooth - these will be processed synchronously until they are all completed
//valid script symbols are:
//	POWERON | POWEROFF
//	SCANON | SCANOFF
//	PAIR {MAC_ADDR}
//	CONNECT(?)
//	TRUST(?)

void bluetooth_script(t_adapter *x, t_symbol *s, int argc, t_atom *argv) {

  //loop through symbols recieved in argv and process them
  for (int i = 0; i < argc; i++) {
    
    
    
  }

	
	
	
}



//New object, which should have a symbol for a device MAC address passed into it
void *adapter_new(t_symbol *arg) {
  t_adapter *x = (t_adapter *)pd_new(adapter_class);

  if (arg == NULL) {
    error("must provide mac address in object setup");
  } else {
    x->mac_address = arg;
  }

  //outlets
  x->status_outlet = outlet_new(&x->x_obj, &s_float);
   
  return (void *)x;
}



void bluetooth_setup(void) {
  //Create class to represent our object
  bluetooth_class = class_new(gensym("bluetooth"),
	(t_newmethod)bluetooth_new, NULL,
	sizeof(t_bluetooth), CLASS_DEFAULT, A_SYMBOL, 0);

  //Bang to get bluetooth status as a message
  class_addbang(bluetooth_class, bluetooth_bang);

  //Script method which allows user to send a collection of symbols to the object which will then be processed in a single DBus connection
  class_addlist(bluetooth_class, (t_method)bluetooth_script);
  
}


