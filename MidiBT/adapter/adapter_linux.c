#include <stdbool.h>
#include <string.h>

#include "m_pd.h"
#include "../common.h"

#include <glib.h>
#include <gio/gio.h>




static t_class *adapter_class = NULL;

typedef struct _adapter {
  t_object x_obj;

  //Current state;
  bool has_adapter;
  float powered;

  //DBus things
  GDBusConnection *dbusConnection;
  GDBusProxy *adapterProxy;
  GDBusProxy *propsProxy;

  //Outlets
  t_outlet *power_outlet;
  
} t_adapter;  

void adapter_bang(t_adapter *x) {
  (void)x; // silence unused variable warning
  
  post("adapater running live on bluez");
  outlet_bang(x->power_outlet);

}  


//Actual code for calling stuff in binc for adapter handling
float get_power_state(t_adapter *x) {

  GError *error;

  //Read the adapter property - if we fail, we should reset the state of has_adapter to false and fail out
  GVariant *result;
  result = g_dbus_proxy_call_sync(x->propsProxy, "Get", g_variant_new("(ss)", "org.bluez.Adapter1","Powered"), G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
  
  if (error != NULL) {
    post("midiBT: adapter: W013: no Bluetooth adapter found, is one connected? message %s", error->message);
    x->has_adapter = false;
    x->powered = -1;
    return x->powered;
  }
  
  x->has_adapter = true;
  
  const gboolean *powered;
  GVariant *unpack;
  g_variant_get(result, "(v)", &unpack);
  g_variant_get(unpack, "b", &powered);
  
  x->powered = powered ? 1 : 0; 
  
  g_variant_unref(result);
  
  post("MidiBT: adapater: found BT adapter and powered is %d", x->powered);

  return x->powered;

}

float set_power_state(t_adapter *x, bool powerOn) {

  GError *error;

  //Use the property proxy to set powered to required state (on or off)
  g_dbus_proxy_call_sync(x->propsProxy, "Set", g_variant_new("(ssv)","org.bluez.Adapter1","Powered",g_variant_new("b", powerOn)), G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);

  if (error != NULL) {
    post("MidiBT: adapater: E001: Could not set power state, message was: %s", error->message);
    return -1;
  }
  
  return powerOn ? 1 : 0;

}

void adapter_power(t_adapter *x, t_symbol *arg) {

  //This can only be called once init has been called - so we must check that
  
  if (!x->has_adapter) {
    post("midiBT: adapter: must call init method before power argument %s", arg->s_name);
    outlet_float(x->power_outlet, -1);
    return;
  }
  
  //Handle the arg
  if (strcmp(arg->s_name,"on") == 0) {
    outlet_float(x->power_outlet, set_power_state(x, true));
    return;
  }

  if (strcmp(arg->s_name, "off") == 0) {
    outlet_float(x->power_outlet, set_power_state(x, false));
    return;
  }

  if (strcmp(arg->s_name,"state") == 0) {
    outlet_float(x->power_outlet, get_power_state(x));
    return;
  }

  //unknown argument
  pd_error(x, "Invalid argument %s for method: power", arg->s_name);

}

void adapter_init(t_adapter *x) {
  
  //Used to initialise and check that a bluetooth adapter is connected
  
  GError *error;
  
  x->has_adapter = false;
  x->powered = false;
  
  x->dbusConnection = g_bus_get_sync(G_BUS_TYPE_SYSTEM, NULL, &error);
  g_assert_no_error(error);
  
  if (error != NULL) {
    post("midiBT: adapter: 010: could not init DBUS, message %s", error->message);
    return;
  }  
  
  //Create proxy for adapter
  x->adapterProxy = g_dbus_proxy_new_sync(x->dbusConnection, G_DBUS_PROXY_FLAGS_NONE, NULL, "org.bluez", "/org/bluez/hci0", "org.bluez.Adapter1", NULL, &error);
  
  if (error != NULL) {
    post("midiBT: adapter: 011: could not init adapter proxy, message %s", error->message);
    return;
  }  
  
  //Create proxy for properties
  x->propsProxy = g_dbus_proxy_new_sync(x->dbusConnection, G_DBUS_PROXY_FLAGS_NONE, NULL, "org.bluez", "/org/bluez/hci0", "org.freedesktop.DBus.Properties", NULL, &error);
  
  if (error != NULL) {
    post("midiBT: adapter: 012: could not init properties proxy, message %s", error->message);
    return;
  }  
  
  x->powered = get_power_state(x);
  
  if (x->powered == -1) {
    post("midiBT: adapter: 013: no Bluetooth adapter found, is one connected? message %s", error->message);
    return;    
  }
  
  post("MidiBT: adapater: found BT adapter and powered is %d", x->powered);
  
}







void *adapter_new(void) {
  t_adapter *x = (t_adapter *)pd_new(adapter_class);

//   GError *error;
// 
//   //init dbus, and check adapter is present by trying to get the powered state
//   x->dbusConnection = g_bus_get_sync(G_BUS_TYPE_SYSTEM, NULL, &error);
//   g_assert_no_error(error);

  //outlets
  x->power_outlet = outlet_new(&x->x_obj, &s_float);



 
   
  return (void *)x;
}

void adapter_setup(void) {
  //Create class to represent our object
  adapter_class = class_new(gensym("adapter"),
    (t_newmethod)adapter_new, NULL,
    sizeof(t_adapter), CLASS_DEFAULT, 0);


  //Bang to get adapter status
  class_addbang(adapter_class, adapter_bang);

  //Init method, starts DBus connection, and checks power state
  class_addmethod(adapter_class, (t_method)adapter_init, gensym("init"), 0);

  //Power method, use to get status, turn on and off (messages on, off, state)
  class_addmethod(adapter_class,(t_method)adapter_power, gensym("power"), A_SYMBOL, 0);
  
}




//    
//    g_assert_no_error(error);
// // 
//    post("created proxy");
// 
//    GVariant *value;
// 
//    //Lets try reading the powereed state
//    value = g_dbus_proxy_get_cached_property(x->bluezProxy, "Address");
// 
//    const gchar *addr;
//    
//    g_variant_get(value, "s", &addr);
//    
//    post("address %s", addr);
//    
//    g_variant_unref(value);
// 
//    value = g_dbus_proxy_get_cached_property(x->bluezProxy, "Powered");
//    
//     const gboolean *pow;
//     
//     g_variant_get(value, "b", &pow);
//     
//     post("powered %d", pow);
//     
//     g_variant_unref(value);
// 
//     GVariant *setValue = g_variant_new("(ssv)","org.bluez.Adapter1","Powered",g_variant_new("b", true));
// 
//     //Try and set powered
//     g_dbus_connection_call_sync(x->dbusConnection, "org.bluez", "/org/bluez/hci0", "org.freedesktop.DBus.Properties", "Set", setValue, NULL, G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
//     g_assert_no_error(error);
//   
//   
//     g_variant_get(value, "b", &pow);
//     
//     post("powered %d", pow);
//     
//     g_variant_unref(value);