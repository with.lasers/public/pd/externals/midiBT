/**

   with-lasers.studio / lxinspc

   project name: MidiBT   - a library of Pure Data externals for using Bluetooth and MIDI data
   file name: adapter_stub.c

   created: 16/11/2023

  The adapter object as a stub - i.e. no bluetooth functionality, and simulations of the various functions for testing
  and patch development on other systems

  TODO: add support for all environments (windows and Mac OS)
   

*/




#include <stdbool.h>
#include <string.h>

#include "m_pd.h"
#include "../common.h"


static t_class *adapter_class = NULL;

typedef struct _adapter {
  t_object x_obj;
  //are we simulating for non linux platforms
  float power;
  //Outlets
  t_outlet *power_outlet;
} t_adapter;  

//When adapter object is banged, we post current status to console
void adapter_bang(t_adapter *x) {
  (void)x; // silence unused variable warning
  


  post("adapter: running as emulation");    

}  

//Utility method to get power state - as a simulation we return the status of power
float get_power_state(t_adapter *x) {
  
  return x->power;

}


//Power method, must have an argument - and this must be either on | off | state
void adapter_power(t_adapter *x, t_symbol *arg) {

  //Handle the arg
  if (strcmp(arg->s_name,"on") == 0) {
    x->power = 1;
    outlet_float(x->power_outlet, x->power);
    return;
  }

  if (strcmp(arg->s_name, "off") == 0) {
    x->power = 0;
    outlet_float(x->power_outlet, x->power);
    return;
  }

  if (strcmp(arg->s_name,"state") == 0) {
    outlet_float(x->power_outlet, get_power_state(x));
    return;
  }

  //unkown argument
  pd_error(x, "Invalid argument %s for method: power", arg->s_name);

}



void *adapter_new(void) {
  t_adapter *x = (t_adapter *)pd_new(adapter_class);

  logpost(x, 3, "adapter: running as emulation, as requires linux");
  x->power = 0;

  //outlets
  x->power_outlet = outlet_new(&x->x_obj, &s_float);

  return (void *)x;
}

void adapter_setup(void) {
  //Create class to represent our object
  adapter_class = class_new(gensym("adapter"),
    (t_newmethod)adapter_new, NULL,
    sizeof(t_adapter), CLASS_DEFAULT, 0);
  class_addbang(adapter_class, adapter_bang);

  //Power on method
  class_addmethod(adapter_class,(t_method)adapter_power, gensym("power"), A_SYMBOL, 0);

}
