#include "m_pd.h"

static t_class *device_class = NULL;

typedef struct _device {
  t_object x_obj;
} t_device;  

void device_bang(t_device *x) {
  (void)x; // silence unused variable warning
  


  #ifdef UNIX
    #ifdef MACOSX
      post("Hello MAC OSX");
    #else
      post("Hello unix / linux");
    #endif
  #endif
}  


// void device_on(t_device *x) {
//   (void)x; // silence unused variable warning
//   #ifdef UNIX
//     #ifdef MACOSX
//       post("MidiBT: Simulate power on");
//     #else
//       //TODO: DBus code to turn bluetooth power on
//     #endif
//   #endif
// }

// void device_off(t_device *x) {
//   (void)x; // silence unused variable warning
//   #ifdef UNIX
//     #ifdef MACOSX
//       post("MidiBT: Simulate power off");
//     #else
//       //TODO: DBus code to turn bluetooth device on
//     #endif
//   #endif
// }


void *device_new(void) {
  t_device *x = (t_device *)pd_new(device_class);
  return (void *)x;
}

void device_setup(void) {
  //Create class to represent our object
  device_class = class_new(gensym("device"),
    (t_newmethod)device_new, NULL,
    sizeof(t_device), CLASS_DEFAULT, 0);
  class_addbang(device_class, device_bang);

  // //device on method
  // class_addmethod(device_class,(t_method)device_on, gensym("on"), 0);

  // //device off method
  // class_addmethod(device_class,(t_method)device_off, gensym("off"),0);

}
