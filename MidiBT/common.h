/**

   with-lasers.studio / lxinspc

   project name: MidiBT   - a library of Pure Data externals for using Bluetooth and MIDI data
   file name: common.h

   created: 16/11/2023

  The adapter object as a stub - i.e. no bluetooth functionality, and simulations of the various functions for testing
  and patch development on other systems

  TODO: add support for all environments (windows and Mac OS)
   

*/

#pragma once

#define PD_LOG_FATAL 0
#define PD_LOG_ERROR 1
#define PD_LOG_NORMAL 2
#define PD_LOG_VERBOSE 3
#define PD_LOG_MODE_VERBOSE 4
