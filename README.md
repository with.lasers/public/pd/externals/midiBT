# MidiBT - A library of PD Externals for connecting to MIDI over BLE using a Bela





## PD Objects

### Bluetooth

Core object which manages the bluetooth connection

Must be created with a parameter of the mac address of the device being connected to

Can then be passed a list which acts as a script for the connection, which supports the following symbols

* POW_ON
* POW_OFF
* SCAN
* PAIR
* CONNECT










